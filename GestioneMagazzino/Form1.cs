﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data.SQLite;
using System.Reflection;
using System.Diagnostics;

namespace GestioneMagazzino
{
    public partial class Form1 : Form
    {
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", SetLastError = true, CallingConvention = CallingConvention.Winapi)] 
	    [return: MarshalAs(UnmanagedType.Bool)] 
	    public static extern bool IsWow64Process([In] IntPtr hProcess, [Out] out bool lpSystemInfo); 
	 
     	 public bool Is64Bit() 
	    {
            bool retVal; 
	        IsWow64Process(Process.GetCurrentProcess().Handle, out retVal); 
	 
	        return retVal; 
	    } 


        struct Scaffale
        {
           public string Nome;
           public int ID;
           public string Nota;
        }
        List<Scaffale> ListaScaffali;

        int IDScaffaleSelez;
        static int rigaselez;
        static string indirizzoDB = @"DBMagazzino.s3db";
        public Form1()
        {
            InitializeComponent();

        }
        ListViewGroup GruppoScaffali = new ListViewGroup("Scaffali");

        private void Form1_Load(object sender, EventArgs e)
        {
            lstw_scaffali.Groups.Add(GruppoScaffali);
            ListViewItem_SetSpacing(this.lstw_scaffali, 130+64,44+56);

           ListaScaffali = OttieniListaScaffali(indirizzoDB);

          

            lstw_scaffali.Items.Clear();

            
            
           foreach (Scaffale scaffalecorr in ListaScaffali)
           {              
               lstw_scaffali.Items.Add(scaffalecorr.Nome,0);
               lstw_scaffali.Items[lstw_scaffali.Items.Count - 1].Group = GruppoScaffali;
               lstw_scaffali.Items[lstw_scaffali.Items.Count - 1].ToolTipText = scaffalecorr.Nota;               
           }           
        }
        
        private void Click_Celle(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgw_Scaffale.Columns["cln_cancellaelemento"].Index && e.RowIndex >= 0)
            {

                Point punti=PointToScreen(((Control)sender).Location);
                rigaselez = e.RowIndex;
                tls_riga_opzioni.Show(MousePosition.X, MousePosition.Y);
            }
        }

        Quantita FormQuantita;
        private void tls_riga_opzioni_AggiungiRimuoviQuant_Click(object sender, EventArgs e)
        {
            string nomeogg = dgw_Scaffale.Rows[rigaselez].Cells[2].Value.ToString();
            int quantita=Convert.ToInt16(dgw_Scaffale.Rows[rigaselez].Cells[3].Value);
            string nota = dgw_Scaffale.Rows[rigaselez].Cells[4].Value.ToString();

            FormQuantita=new Quantita(nomeogg, quantita, nota);
            FormQuantita.Show();
            Quantita.ActiveForm.FormClosed += new FormClosedEventHandler(FormQuantitaChiuso);
        }

        private void FormQuantitaChiuso(object sender, EventArgs e)
        {
            if (FormQuantita.valido)
            {
                if (FormQuantita.modalitàform == Quantita.Modalità.Modifica)
                {
                    int ID = Convert.ToInt16(dgw_Scaffale[0, rigaselez].Value);
                    string sql = "UPDATE Oggetto SET Nome =\"" + FormQuantita.nomeoggetto + "\", NOTE =\"" + FormQuantita.nota + "\", Quantita =\"" + FormQuantita.quantita + "\" WHERE ID=" + ID + ";";
                    EseguiSQL(indirizzoDB, sql);
                    RefreshScaffale(IDScaffaleSelez);
                }
                if (FormQuantita.modalitàform == Quantita.Modalità.Nuovo)
                {
                    string sql = "INSERT INTO Oggetto (Nome,Quantita,Note,ID_Scaffale) VALUES (\"" + FormQuantita.nomeoggetto + "\", " + FormQuantita.quantita + ",\"" + FormQuantita.nota + "\"," + IDScaffaleSelez + ");";
                    EseguiSQL(indirizzoDB, sql);
                    RefreshScaffale(IDScaffaleSelez);
                }
             }        
        }

       

        private void tls_riga_opzioni_CancellaRiga_Click(object sender, EventArgs e)
        {
            DialogResult Risultato = MessageBox.Show("Sicuro di voler Eliminare questo Oggetto?\n\n L'operazione non sarà invertibile!", "Attenzione!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (Risultato == System.Windows.Forms.DialogResult.Yes)
            {
                int ID = Convert.ToInt16(dgw_Scaffale[0, rigaselez].Value);
                string sql = "DELETE FROM Oggetto WHERE ID=" + ID + ";";
                EseguiSQL(indirizzoDB, sql);
                RefreshScaffale(IDScaffaleSelez);
            }

        }

        private void tls_riga_opzioni_sposta_conferma_Click(object sender, EventArgs e)
        {
            int scaffaleselezcmb = tls_riga_opzioni_sposta_ScegliScaffale.SelectedIndex;
            int ID = Convert.ToInt16(dgw_Scaffale[0, rigaselez].Value);
            string sql = "UPDATE Oggetto SET ID_Scaffale = " + ListaScaffali[scaffaleselezcmb].ID + " WHERE ID=" + ID + ";";
            EseguiSQL(indirizzoDB, sql);
            RefreshScaffale(IDScaffaleSelez);

        }

        #region CorrelatiSQL
        private void RefreshScaffale(int IDscaffale)
        {
            dgw_Scaffale.Rows.Clear();
           
            string sql = "SELECT Oggetto.ID,Scaffale.Nome,Oggetto.Nome,Oggetto.Quantita,Oggetto.Note FROM OGGETTO INNER JOIN SCAFFALE ON Oggetto.ID_Scaffale=Scaffale.ID WHERE ID_Scaffale = " + IDscaffale + ";";
            var conn = new SQLiteConnection("Data Source=" + indirizzoDB + ";Version=3;");
            try
            {
                conn.Open();
                DataSet ds = new DataSet();
                var da = new SQLiteDataAdapter(sql, conn);
                da.Fill(ds);


                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    dgw_Scaffale.Rows.Add();
                    for (int colonna = 0; colonna < ds.Tables[0].Columns.Count; colonna++)
                        dgw_Scaffale.Rows[i].Cells[colonna].Value = (ds.Tables[0].Rows[i])[colonna];
                }

            }
            catch (Exception)
            {
                throw;
            }
        }

        private void EseguiSQL(string indirizzoDB, string SQL)
        {
            var conn = new SQLiteConnection("Data Source=" + indirizzoDB + ";Version=3;");
            try
            {
                conn.Open();
                DataSet ds = new DataSet();
                var da = new SQLiteDataAdapter(SQL, conn);
                da.Fill(ds);

            }
            catch (Exception)
            {
                throw;
            }
        }

        private List<Scaffale> OttieniListaScaffali(string indirizzoDB)
        {
            List<Scaffale> listascaffali = new List<Scaffale>();
           
            string sql = "Select * FROM Scaffale;";
            var conn = new SQLiteConnection("Data Source=" + indirizzoDB + ";Version=3;");
            try
            {
                conn.Open();
                DataSet ds = new DataSet();
                var da = new SQLiteDataAdapter(sql, conn);
                da.Fill(ds);

                
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                        Scaffale nuovoscaffale;
                        nuovoscaffale.ID = Convert.ToInt16((ds.Tables[0].Rows[i])[0]);           
                        nuovoscaffale.Nome=ds.Tables[0].Rows[i][1].ToString();
                        nuovoscaffale.Nota = ds.Tables[0].Rows[i][2].ToString();
                        listascaffali.Add(nuovoscaffale);
                    
                }

            }
            catch (Exception)
            {
                throw;
            }

            for (int i = 0; i < listascaffali.Count; i++)
            {
                for (int i2 = i + 1; i2 < listascaffali.Count; i2++)
                {
                    if (listascaffali[i].Nome.CompareTo(listascaffali[i2].Nome) > 0)
                    {
                        Scaffale tmp = listascaffali[i];
                        listascaffali[i] = listascaffali[i2];
                        listascaffali[i2] = tmp;
                    }
                }
            }
            return listascaffali;
        }
        #endregion
        
        #region Configurazioni Grafiche on Load

        public int MakeLong(short lowPart, short highPart)
        {
            return (int)(((ushort)lowPart) | (uint)(highPart << 16));
        }
        public void ListViewItem_SetSpacing(ListView listview, short leftPadding, short topPadding)
        {
            const int LVM_FIRST = 0x1000;
            const int LVM_SETICONSPACING = LVM_FIRST + 53;
            SendMessage(listview.Handle, LVM_SETICONSPACING, IntPtr.Zero, (IntPtr)MakeLong(leftPadding, topPadding));
        }
        private void dgw_Scaffale_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dgw_Scaffale.Columns[e.ColumnIndex].Name == "cln_cancellaelemento")
            {
                e.Value = ">";
            }

            if (dgw_Scaffale.Columns[e.ColumnIndex].Name == "cln_diminuisci")
            {
                e.Value = "<-";
            }

            if (dgw_Scaffale.Columns[e.ColumnIndex].Name == "cln_aumenta")
            {
                e.Value = "->";
            }

        }

        #endregion

        private void lstw_scaffali_SelectedIndexChanged(object sender, EventArgs e)
        {
            lbl_nessunelemento.Visible = false;
            tls_nuovo_aggiungiogg.Enabled = true;
            tls_nuovo_aggiorna.Enabled = true;
            if (lstw_scaffali.SelectedIndices.Count > 0)
            {
                int indice = lstw_scaffali.SelectedIndices[0];
                RefreshScaffale(ListaScaffali[indice].ID);
                IDScaffaleSelez = ListaScaffali[indice].ID;
                lstw_scaffali.FocusedItem.Focused = false;
            }
        }
       

        private void tls_riga_opzioni_Opened(object sender, EventArgs e)
        {
            tls_riga_opzioni_sposta_ScegliScaffale.Items.Clear();

            for(int i=0; i < ListaScaffali.Count;i++)
                if (ListaScaffali[i].ID != IDScaffaleSelez)
                    tls_riga_opzioni_sposta_ScegliScaffale.Items.Add(ListaScaffali[i].Nome);
                 else
                     tls_riga_opzioni_sposta_ScegliScaffale.Items.Add(ListaScaffali[i].Nome+" (corrente)");

            if (tls_riga_opzioni_sposta_ScegliScaffale.Items.Count != 0) // se ci sono elementi, mostra il primo
                tls_riga_opzioni_sposta_ScegliScaffale.Text = tls_riga_opzioni_sposta_ScegliScaffale.Items[0].ToString();
            else
                tls_riga_opzioni_sposta_ScegliScaffale.Text = "Nessun altro scaffale presente!";

         
        }

        private void tls_nuovo_aggiungiogg_Click(object sender, EventArgs e)
        {
            FormQuantita = new Quantita();
            FormQuantita.Show();
            Quantita.ActiveForm.FormClosed += new FormClosedEventHandler(FormQuantitaChiuso);
        }

        private void tls_Scaffali_Opened(object sender, EventArgs e)
        {
            if (ListaScaffali.Count == 0)
            {
                tls_scaffali_modifica.Enabled = false;
                tls_scaffali_elimina.Enabled = false;
            }
            else
            {
                tls_scaffali_modifica.Enabled = true;
                tls_scaffali_elimina.Enabled = true;

                tls_scaffali_elimina_scegliscaffale.Items.Clear();
                tls_scaffali_modifica_scegliscaffale.Items.Clear();

                for (int i = 0; i < ListaScaffali.Count; i++)
                    if (ListaScaffali[i].ID != IDScaffaleSelez)
                    {
                        tls_scaffali_elimina_scegliscaffale.Items.Add(ListaScaffali[i].Nome);
                        tls_scaffali_modifica_scegliscaffale.Items.Add(ListaScaffali[i].Nome);
                    }
                    else
                    {
                        tls_scaffali_elimina_scegliscaffale.Items.Add(ListaScaffali[i].Nome + " (corrente)");
                        tls_scaffali_modifica_scegliscaffale.Items.Add(ListaScaffali[i].Nome + " (corrente)");
                    }


                if (tls_scaffali_elimina_scegliscaffale.Items.Count != 0) // se ci sono elementi, mostra il primo
                {
                    tls_scaffali_elimina_scegliscaffale.Text = tls_scaffali_elimina_scegliscaffale.Items[0].ToString();
                    tls_scaffali_modifica_scegliscaffale.Text = tls_scaffali_modifica_scegliscaffale.Items[0].ToString();
                }
                else
                {
                    tls_scaffali_elimina_scegliscaffale.Text = "Nessun altro scaffale presente!";
                    tls_scaffali_modifica_scegliscaffale.Text = "Nessun altro scaffale presente!";
                }
            }
           
         
        }

        


        #region modifica/agggiungi/rimuovi Scaffale
        private void tls_scaffali_modifica_conferma_Click(object sender, EventArgs e)
        {
            formscaffale = new FormScaffale(ListaScaffali[tls_scaffali_modifica_scegliscaffale.SelectedIndex].Nome, ListaScaffali[tls_scaffali_modifica_scegliscaffale.SelectedIndex].Nota, ListaScaffali[tls_scaffali_modifica_scegliscaffale.SelectedIndex].ID);
            formscaffale.Show();
            formscaffale.FormClosed += new FormClosedEventHandler(FormScaffaleChiuso);
        }

        private void tls_scaffali_elimina_conferma_Click(object sender, EventArgs e)
        {
            DialogResult Risultato=MessageBox.Show("Sicuro di voler Eliminare questo scaffale e tutti gli oggetti che contiene?\n\n L'operazione non sarà invertibile!", "Attenzione!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (Risultato == System.Windows.Forms.DialogResult.Yes)
            {
                string sql;
                sql = "DELETE FROM Scaffale WHERE ID=" + ListaScaffali[tls_scaffali_elimina_scegliscaffale.SelectedIndex].ID + ";";
                EseguiSQL(indirizzoDB, sql);

                sql = "DELETE FROM Oggetto WHERE ID_Scaffale=" + ListaScaffali[tls_scaffali_elimina_scegliscaffale.SelectedIndex].ID + ";";
                EseguiSQL(indirizzoDB, sql);

                lstw_scaffali.Items.Clear();
                ListaScaffali = OttieniListaScaffali(indirizzoDB);
                lstw_scaffali.ShowItemToolTips = true;

                foreach (Scaffale scaffalecorr in ListaScaffali)
                {
                    lstw_scaffali.Items.Add(scaffalecorr.Nome, 0);
                    lstw_scaffali.Items[lstw_scaffali.Items.Count - 1].ToolTipText = scaffalecorr.Nota;
                }

                dgw_Scaffale.Rows.Clear();
                lbl_nessunelemento.Visible = true;
            }
        }

        FormScaffale formscaffale;
        private void tls_scaffali_aggiungi_Click(object sender, EventArgs e)
        {
            formscaffale = new FormScaffale();
            formscaffale.Show();
            formscaffale.FormClosed += new FormClosedEventHandler(FormScaffaleChiuso);
        }

        private void FormScaffaleChiuso(object sender, EventArgs e)
        {
            if (formscaffale.valido)
            {
                string sql = "";
                if (formscaffale.modalitàform == FormScaffale.Modalità.Modifica)                
                    sql = "UPDATE Scaffale SET Nome =\"" + formscaffale.nome + "\", Note =\"" + formscaffale.nota + "\" WHERE ID=" + formscaffale.ID + ";";
          
                if (formscaffale.modalitàform == FormScaffale.Modalità.Nuovo)                
                    sql = "INSERT INTO Scaffale (Nome,Note) VALUES (\"" + formscaffale.nome + "\",\"" + formscaffale.nota + "\");";
                   
                
                EseguiSQL(indirizzoDB, sql); 
                lstw_scaffali.Items.Clear();
                ListaScaffali = OttieniListaScaffali(indirizzoDB);
                foreach (Scaffale scaffalecorr in ListaScaffali)
                {
                    lstw_scaffali.Items.Add(scaffalecorr.Nome, 0);
                    lstw_scaffali.Items[lstw_scaffali.Items.Count - 1].Group = GruppoScaffali;
                    lstw_scaffali.Items[lstw_scaffali.Items.Count - 1].ToolTipText = scaffalecorr.Nota;
               }   
            }
        }

        #endregion

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Per poter Aggiungere uno Scaffale, premi click destro sullo spazio degli scaffali e \"aggiungi scaffale\".\n\nPer poter aggiungere un oggetto, premi click destro sullo spazio oggetti dopo aver selezionato uno scaffale, e \"Aggiungi Oggetto\".\n\nInfine, per modificare un oggetto esistente, cliccka sulla frecciettina a margine di ogni oggetto.", "Informazioni", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void creditiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Software Gestionale a cura di Stefano Belli.\nBelli.Stefano.Dev@gmail.com\n\n Tutti i Diritti Riservati", "Informazioni", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void cancellaOggettiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult Risultato = MessageBox.Show("Sicuro di voler Eliminare tutti gli Oggetti del Database?\n\n L'operazione non sarà invertibile!", "Attenzione!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (Risultato == System.Windows.Forms.DialogResult.Yes)
            {
                string sql = "DELETE FROM Oggetto";
                EseguiSQL(indirizzoDB, sql);

                dgw_Scaffale.Rows.Clear();

                 sql = "DELETE FROM sqlite_sequence WHERE name = 'Oggetto';";
                EseguiSQL(indirizzoDB, sql);

            }
        }

        private void cancellaScaffaliToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult Risultato = MessageBox.Show("Sicuro di voler Eliminare tutti gli Scaffali e i relativi oggetti dal Database?\n\n L'operazione non sarà invertibile!", "Attenzione!", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (Risultato == System.Windows.Forms.DialogResult.Yes)
            {
                string sql = "DELETE FROM Oggetto";
                EseguiSQL(indirizzoDB, sql);

                sql = "DELETE FROM Scaffale";
                EseguiSQL(indirizzoDB, sql);

                sql = "DELETE FROM sqlite_sequence WHERE name = 'Oggetto';";
                EseguiSQL(indirizzoDB, sql);

                sql = "DELETE FROM sqlite_sequence WHERE name = 'Scaffale';";
                EseguiSQL(indirizzoDB, sql);

                dgw_Scaffale.Rows.Clear();
                lstw_scaffali.Items.Clear();

                ListaScaffali.Clear();
                tls_scaffali_elimina_scegliscaffale.Items.Clear();
                tls_scaffali_modifica_scegliscaffale.Items.Clear();
            }
        }

        private void esciToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult we=MessageBox.Show("Sicuro di voler Uscire?", "Conferma", MessageBoxButtons.YesNo);
            
            
            this.Close();
        }
         

    }
}
