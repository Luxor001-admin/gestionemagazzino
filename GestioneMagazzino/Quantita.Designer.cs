﻿namespace GestioneMagazzino
{
    partial class Quantita
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_Nome = new System.Windows.Forms.Label();
            this.btn_conferma = new System.Windows.Forms.Button();
            this.btn_annulla = new System.Windows.Forms.Button();
            this.txt_nome = new System.Windows.Forms.TextBox();
            this.txt_nota = new System.Windows.Forms.TextBox();
            this.lbl_note = new System.Windows.Forms.Label();
            this.lbl_quantita = new System.Windows.Forms.Label();
            this.txt_numero = new System.Windows.Forms.TextBox();
            this.txt_tipo = new System.Windows.Forms.TextBox();
            this.txt_modificaQuant = new System.Windows.Forms.TextBox();
            this.lbl_quantitatemp = new System.Windows.Forms.Label();
            this.btn_confermaquant = new System.Windows.Forms.Button();
            this.btn_diminuisci = new System.Windows.Forms.Button();
            this.btn_aumenta = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_Nome
            // 
            this.lbl_Nome.AutoSize = true;
            this.lbl_Nome.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_Nome.Location = new System.Drawing.Point(13, 9);
            this.lbl_Nome.Name = "lbl_Nome";
            this.lbl_Nome.Size = new System.Drawing.Size(51, 20);
            this.lbl_Nome.TabIndex = 0;
            this.lbl_Nome.Text = "Nome";
            // 
            // btn_conferma
            // 
            this.btn_conferma.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_conferma.Location = new System.Drawing.Point(13, 258);
            this.btn_conferma.Name = "btn_conferma";
            this.btn_conferma.Size = new System.Drawing.Size(236, 53);
            this.btn_conferma.TabIndex = 9;
            this.btn_conferma.Text = "Conferma";
            this.btn_conferma.UseVisualStyleBackColor = true;
            this.btn_conferma.Click += new System.EventHandler(this.btn_conferma_Click);
            // 
            // btn_annulla
            // 
            this.btn_annulla.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_annulla.Location = new System.Drawing.Point(255, 275);
            this.btn_annulla.Name = "btn_annulla";
            this.btn_annulla.Size = new System.Drawing.Size(72, 36);
            this.btn_annulla.TabIndex = 10;
            this.btn_annulla.Text = "Annulla";
            this.btn_annulla.UseVisualStyleBackColor = true;
            this.btn_annulla.Click += new System.EventHandler(this.btn_annulla_Click);
            // 
            // txt_nome
            // 
            this.txt_nome.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nome.Location = new System.Drawing.Point(70, 6);
            this.txt_nome.Name = "txt_nome";
            this.txt_nome.Size = new System.Drawing.Size(257, 26);
            this.txt_nome.TabIndex = 4;
            this.txt_nome.Text = "Togo";
            this.txt_nome.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_nota
            // 
            this.txt_nota.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_nota.Location = new System.Drawing.Point(72, 167);
            this.txt_nota.Multiline = true;
            this.txt_nota.Name = "txt_nota";
            this.txt_nota.Size = new System.Drawing.Size(257, 85);
            this.txt_nota.TabIndex = 8;
            // 
            // lbl_note
            // 
            this.lbl_note.AutoSize = true;
            this.lbl_note.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_note.Location = new System.Drawing.Point(13, 167);
            this.lbl_note.Name = "lbl_note";
            this.lbl_note.Size = new System.Drawing.Size(43, 20);
            this.lbl_note.TabIndex = 5;
            this.lbl_note.Text = "Note";
            // 
            // lbl_quantita
            // 
            this.lbl_quantita.AutoSize = true;
            this.lbl_quantita.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_quantita.Location = new System.Drawing.Point(9, 78);
            this.lbl_quantita.Name = "lbl_quantita";
            this.lbl_quantita.Size = new System.Drawing.Size(70, 20);
            this.lbl_quantita.TabIndex = 7;
            this.lbl_quantita.Text = "Quantità";
            // 
            // txt_numero
            // 
            this.txt_numero.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_numero.Location = new System.Drawing.Point(110, 65);
            this.txt_numero.Name = "txt_numero";
            this.txt_numero.Size = new System.Drawing.Size(100, 38);
            this.txt_numero.TabIndex = 6;
            this.txt_numero.Text = "70";
            this.txt_numero.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_tipo
            // 
            this.txt_tipo.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_tipo.Location = new System.Drawing.Point(110, 106);
            this.txt_tipo.Name = "txt_tipo";
            this.txt_tipo.Size = new System.Drawing.Size(100, 22);
            this.txt_tipo.TabIndex = 7;
            this.txt_tipo.TabStop = false;
            this.txt_tipo.Text = "Pezzi";
            this.txt_tipo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txt_modificaQuant
            // 
            this.txt_modificaQuant.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_modificaQuant.Location = new System.Drawing.Point(243, 79);
            this.txt_modificaQuant.Name = "txt_modificaQuant";
            this.txt_modificaQuant.Size = new System.Drawing.Size(64, 22);
            this.txt_modificaQuant.TabIndex = 17;
            this.txt_modificaQuant.TabStop = false;
            this.txt_modificaQuant.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txt_modificaQuant.TextChanged += new System.EventHandler(this.txt_modificaQuant_TextChanged);
            // 
            // lbl_quantitatemp
            // 
            this.lbl_quantitatemp.AutoSize = true;
            this.lbl_quantitatemp.Location = new System.Drawing.Point(282, 106);
            this.lbl_quantitatemp.Name = "lbl_quantitatemp";
            this.lbl_quantitatemp.Size = new System.Drawing.Size(25, 13);
            this.lbl_quantitatemp.TabIndex = 18;
            this.lbl_quantitatemp.Text = "(50)";
            this.lbl_quantitatemp.Visible = false;
            // 
            // btn_confermaquant
            // 
            this.btn_confermaquant.BackColor = System.Drawing.Color.Transparent;
            this.btn_confermaquant.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_confermaquant.Location = new System.Drawing.Point(308, 78);
            this.btn_confermaquant.Name = "btn_confermaquant";
            this.btn_confermaquant.Size = new System.Drawing.Size(31, 23);
            this.btn_confermaquant.TabIndex = 19;
            this.btn_confermaquant.TabStop = false;
            this.btn_confermaquant.UseVisualStyleBackColor = false;
            this.btn_confermaquant.Click += new System.EventHandler(this.btn_confermaquant_Click);
            // 
            // btn_diminuisci
            // 
            this.btn_diminuisci.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_diminuisci.Location = new System.Drawing.Point(212, 92);
            this.btn_diminuisci.Name = "btn_diminuisci";
            this.btn_diminuisci.Size = new System.Drawing.Size(25, 25);
            this.btn_diminuisci.TabIndex = 11;
            this.btn_diminuisci.TabStop = false;
            this.btn_diminuisci.UseVisualStyleBackColor = true;
            this.btn_diminuisci.Click += new System.EventHandler(this.btn_diminuisci_Click);
            // 
            // btn_aumenta
            // 
            this.btn_aumenta.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_aumenta.Location = new System.Drawing.Point(212, 65);
            this.btn_aumenta.Name = "btn_aumenta";
            this.btn_aumenta.Size = new System.Drawing.Size(25, 25);
            this.btn_aumenta.TabIndex = 10;
            this.btn_aumenta.TabStop = false;
            this.btn_aumenta.UseVisualStyleBackColor = true;
            this.btn_aumenta.Click += new System.EventHandler(this.btn_aumenta_Click);
            // 
            // Quantita
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(341, 318);
            this.Controls.Add(this.btn_confermaquant);
            this.Controls.Add(this.lbl_quantitatemp);
            this.Controls.Add(this.txt_modificaQuant);
            this.Controls.Add(this.btn_diminuisci);
            this.Controls.Add(this.btn_aumenta);
            this.Controls.Add(this.txt_tipo);
            this.Controls.Add(this.txt_numero);
            this.Controls.Add(this.lbl_quantita);
            this.Controls.Add(this.txt_nota);
            this.Controls.Add(this.lbl_note);
            this.Controls.Add(this.txt_nome);
            this.Controls.Add(this.btn_annulla);
            this.Controls.Add(this.btn_conferma);
            this.Controls.Add(this.lbl_Nome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Quantita";
            this.Text = "Modifica Oggetto";
            this.Load += new System.EventHandler(this.Quantita_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_Nome;
        private System.Windows.Forms.Button btn_conferma;
        private System.Windows.Forms.Button btn_annulla;
        private System.Windows.Forms.TextBox txt_nome;
        private System.Windows.Forms.TextBox txt_nota;
        private System.Windows.Forms.Label lbl_note;
        private System.Windows.Forms.Label lbl_quantita;
        private System.Windows.Forms.TextBox txt_numero;
        private System.Windows.Forms.TextBox txt_tipo;
        private System.Windows.Forms.Button btn_aumenta;
        private System.Windows.Forms.Button btn_diminuisci;
        private System.Windows.Forms.TextBox txt_modificaQuant;
        private System.Windows.Forms.Label lbl_quantitatemp;
        private System.Windows.Forms.Button btn_confermaquant;
    }
}