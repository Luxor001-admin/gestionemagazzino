﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace GestioneMagazzino
{
    public partial class Quantita : Form
    {
        public string nomeoggetto;
        public int quantita;
        public string nota;
        public bool valido = true;
        public Modalità modalitàform;
        public enum Modalità
        {
            Modifica,
            Nuovo
        }
        public Quantita(string nomeoggetto,int quantita,string nota)
        {
            this.nomeoggetto = nomeoggetto;
            this.quantita = quantita;
            this.nota = nota;
            modalitàform = Modalità.Modifica;
            InitializeComponent();
        }
        public Quantita(string nomeoggetto, int quantita, string nota,string tipoogg)
        {
            modalitàform = Modalità.Modifica;
            InitializeComponent();
            this.Text = "Modifica Oggetto";
        }

        public Quantita()
        {
            modalitàform = Modalità.Nuovo;            
            InitializeComponent();
            this.Text = "Aggiungi Oggetto";
        }


        private void Quantita_Load(object sender, EventArgs e)
        {
            txt_nome.Text = nomeoggetto;
            txt_numero.Text = quantita.ToString();
            txt_nota.Text = nota;
        }

        private void btn_aumenta_Click(object sender, EventArgs e)
        {
            quantita++;
            txt_numero.Text = quantita.ToString();
        }

        private void btn_diminuisci_Click(object sender, EventArgs e)
        {
            quantita--;
            txt_numero.Text = quantita.ToString();
        }

        private void btn_conferma_Click(object sender, EventArgs e)
        {
            if (quantita < 0)
            {
                MessageBox.Show("Stai inserendo una quantità minore di 0!", "Errore!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (string.IsNullOrEmpty(txt_nome.Text))
            {
                MessageBox.Show("Nessun Nome Inserito per l'oggetto!", "Errore!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            quantita = Convert.ToInt16(txt_numero.Text);
            nota = txt_nota.Text;
            nomeoggetto = txt_nome.Text;
            valido = true;
            this.Close();
        }

        private void btn_annulla_Click(object sender, EventArgs e)
        {
            valido = false;
            this.Close();
        }

        private void txt_modificaQuant_TextChanged(object sender, EventArgs e)
        {
            

            if (txt_modificaQuant.Text != "")
            {
                if (txt_modificaQuant.Text[0] != '-' && txt_modificaQuant.Text[0] != '+')
                {
                    string stringa = txt_modificaQuant.Text;
                    stringa = stringa.Remove(0, 1);
                    txt_modificaQuant.Text = stringa;
                }

                if (txt_modificaQuant.Text.Length > 1)
                {

                    for(int i=1;i< txt_modificaQuant.Text.Length;i++)
                    {
                        if(!char.IsDigit(txt_modificaQuant.Text[i]))
                        {
                            int posizione = txt_modificaQuant.SelectionStart;
                           txt_modificaQuant.Text= txt_modificaQuant.Text.Remove(i, 1);
                           txt_modificaQuant.SelectionStart = posizione-1;
                        }
                    }             
                }


                if (txt_modificaQuant.Text.Length > 1)
                {
                    btn_confermaquant.Enabled = true;
                    int numero = 0;
                    lbl_quantitatemp.Visible = true;
                    numero = Convert.ToInt16(txt_modificaQuant.Text);


                    numero = quantita + numero;
                    lbl_quantitatemp.Text = "(" + numero.ToString() + ")";


                }
                else
                {
                    lbl_quantitatemp.Visible = false;
                    btn_confermaquant.Enabled = false;
                }
            }
        }

        private void btn_confermaquant_Click(object sender, EventArgs e)
        {

            int numero = 0;
            numero = Convert.ToInt16(txt_modificaQuant.Text);
            quantita += numero;
            txt_numero.Text = quantita.ToString();

            numero = quantita + numero;
            lbl_quantitatemp.Text = "(" + numero.ToString() + ")";
        }
        
    }
}
