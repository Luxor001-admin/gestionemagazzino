﻿using System.Windows.Forms;
namespace GestioneMagazzino
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.ListViewGroup listViewGroup2 = new System.Windows.Forms.ListViewGroup("Scaffali", System.Windows.Forms.HorizontalAlignment.Left);
            System.Windows.Forms.ListViewItem listViewItem5 = new System.Windows.Forms.ListViewItem("A", 0);
            System.Windows.Forms.ListViewItem listViewItem6 = new System.Windows.Forms.ListViewItem("B", 0);
            System.Windows.Forms.ListViewItem listViewItem7 = new System.Windows.Forms.ListViewItem("C", 0);
            System.Windows.Forms.ListViewItem listViewItem8 = new System.Windows.Forms.ListViewItem("D", 0);
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label2 = new System.Windows.Forms.Label();
            this.lstw_scaffali = new System.Windows.Forms.ListView();
            this.tls_Scaffali = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tls_scaffali_aggiungi = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tls_scaffali_modifica = new System.Windows.Forms.ToolStripMenuItem();
            this.tls_scaffali_modifica_scegliscaffale = new System.Windows.Forms.ToolStripComboBox();
            this.tls_scaffali_modifica_conferma = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tls_scaffali_elimina = new System.Windows.Forms.ToolStripMenuItem();
            this.tls_scaffali_elimina_scegliscaffale = new System.Windows.Forms.ToolStripComboBox();
            this.tls_scaffali_elimina_conferma = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.dgw_Scaffale = new System.Windows.Forms.DataGridView();
            this.cln_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_Scaffale = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_nome = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_Qta = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_note = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_DataUlltima = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cln_cancellaelemento = new System.Windows.Forms.DataGridViewButtonColumn();
            this.tls_riga_opzioni = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tls_riga_opzioni_AggiungiRimuoviQuant = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tls_riga_opzioni_CancellaRiga = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.tls_riga_opzioni_Sposta = new System.Windows.Forms.ToolStripMenuItem();
            this.tls_riga_opzioni_sposta_ScegliScaffale = new System.Windows.Forms.ToolStripComboBox();
            this.tls_riga_opzioni_sposta_conferma = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tls_riga_opzioni_Annulla = new System.Windows.Forms.ToolStripMenuItem();
            this.tls_aggiungioggetto = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tls_nuovo_aggiungiogg = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.tls_nuovo_aggiorna = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cancellaOggettiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cancellaScaffaliToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.esciToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aiutoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.creditiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lbl_nessunelemento = new System.Windows.Forms.Label();
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.tls_Scaffali.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgw_Scaffale)).BeginInit();
            this.tls_riga_opzioni.SuspendLayout();
            this.tls_aggiungioggetto.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(222, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(378, 42);
            this.label2.TabIndex = 1;
            this.label2.Text = "Gestione Magazzino";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lstw_scaffali
            // 
            this.lstw_scaffali.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lstw_scaffali.ContextMenuStrip = this.tls_Scaffali;
            this.lstw_scaffali.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstw_scaffali.GridLines = true;
            listViewGroup2.Header = "Scaffali";
            listViewGroup2.Name = "listViewGroup1";
            this.lstw_scaffali.Groups.AddRange(new System.Windows.Forms.ListViewGroup[] {
            listViewGroup2});
            this.lstw_scaffali.ImeMode = System.Windows.Forms.ImeMode.Off;
            listViewItem5.Group = listViewGroup2;
            listViewItem6.Group = listViewGroup2;
            listViewItem7.Group = listViewGroup2;
            listViewItem8.Group = listViewGroup2;
            this.lstw_scaffali.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            listViewItem5,
            listViewItem6,
            listViewItem7,
            listViewItem8});
            this.lstw_scaffali.LargeImageList = this.imageList1;
            this.lstw_scaffali.Location = new System.Drawing.Point(1, 93);
            this.lstw_scaffali.MultiSelect = false;
            this.lstw_scaffali.Name = "lstw_scaffali";
            this.lstw_scaffali.ShowItemToolTips = true;
            this.lstw_scaffali.Size = new System.Drawing.Size(798, 231);
            this.lstw_scaffali.SmallImageList = this.imageList1;
            this.lstw_scaffali.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.lstw_scaffali.TabIndex = 3;
            this.lstw_scaffali.TileSize = new System.Drawing.Size(100, 100);
            this.lstw_scaffali.UseCompatibleStateImageBehavior = false;
            this.lstw_scaffali.SelectedIndexChanged += new System.EventHandler(this.lstw_scaffali_SelectedIndexChanged);
            // 
            // tls_Scaffali
            // 
            this.tls_Scaffali.DropShadowEnabled = false;
            this.tls_Scaffali.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tls_scaffali_aggiungi,
            this.toolStripSeparator5,
            this.tls_scaffali_modifica,
            this.toolStripSeparator6,
            this.tls_scaffali_elimina});
            this.tls_Scaffali.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.tls_Scaffali.Name = "tls_aggiungioggetto";
            this.tls_Scaffali.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tls_Scaffali.ShowImageMargin = false;
            this.tls_Scaffali.Size = new System.Drawing.Size(143, 82);
            this.tls_Scaffali.Opened += new System.EventHandler(this.tls_Scaffali_Opened);
            // 
            // tls_scaffali_aggiungi
            // 
            this.tls_scaffali_aggiungi.Name = "tls_scaffali_aggiungi";
            this.tls_scaffali_aggiungi.Size = new System.Drawing.Size(142, 22);
            this.tls_scaffali_aggiungi.Text = "Aggiungi Scaffale";
            this.tls_scaffali_aggiungi.Click += new System.EventHandler(this.tls_scaffali_aggiungi_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(139, 6);
            // 
            // tls_scaffali_modifica
            // 
            this.tls_scaffali_modifica.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tls_scaffali_modifica_scegliscaffale,
            this.tls_scaffali_modifica_conferma});
            this.tls_scaffali_modifica.Name = "tls_scaffali_modifica";
            this.tls_scaffali_modifica.Size = new System.Drawing.Size(142, 22);
            this.tls_scaffali_modifica.Text = "Modifica Scaffale";
            this.tls_scaffali_modifica.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // tls_scaffali_modifica_scegliscaffale
            // 
            this.tls_scaffali_modifica_scegliscaffale.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.tls_scaffali_modifica_scegliscaffale.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tls_scaffali_modifica_scegliscaffale.Name = "tls_scaffali_modifica_scegliscaffale";
            this.tls_scaffali_modifica_scegliscaffale.Size = new System.Drawing.Size(121, 25);
            // 
            // tls_scaffali_modifica_conferma
            // 
            this.tls_scaffali_modifica_conferma.Name = "tls_scaffali_modifica_conferma";
            this.tls_scaffali_modifica_conferma.Size = new System.Drawing.Size(181, 22);
            this.tls_scaffali_modifica_conferma.Text = "Conferma";
            this.tls_scaffali_modifica_conferma.Click += new System.EventHandler(this.tls_scaffali_modifica_conferma_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(139, 6);
            // 
            // tls_scaffali_elimina
            // 
            this.tls_scaffali_elimina.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tls_scaffali_elimina_scegliscaffale,
            this.tls_scaffali_elimina_conferma});
            this.tls_scaffali_elimina.Name = "tls_scaffali_elimina";
            this.tls_scaffali_elimina.Size = new System.Drawing.Size(142, 22);
            this.tls_scaffali_elimina.Text = "Elimina Scaffale";
            // 
            // tls_scaffali_elimina_scegliscaffale
            // 
            this.tls_scaffali_elimina_scegliscaffale.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.tls_scaffali_elimina_scegliscaffale.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tls_scaffali_elimina_scegliscaffale.Name = "tls_scaffali_elimina_scegliscaffale";
            this.tls_scaffali_elimina_scegliscaffale.Size = new System.Drawing.Size(121, 25);
            // 
            // tls_scaffali_elimina_conferma
            // 
            this.tls_scaffali_elimina_conferma.Name = "tls_scaffali_elimina_conferma";
            this.tls_scaffali_elimina_conferma.Size = new System.Drawing.Size(181, 22);
            this.tls_scaffali_elimina_conferma.Text = "Conferma";
            this.tls_scaffali_elimina_conferma.Click += new System.EventHandler(this.tls_scaffali_elimina_conferma_Click);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "scaffali.jpg");
            this.imageList1.Images.SetKeyName(1, "aggiungi.png");
            this.imageList1.Images.SetKeyName(2, "aggiungi.png");
            this.imageList1.Images.SetKeyName(3, "rimuovi.png");
            // 
            // dgw_Scaffale
            // 
            this.dgw_Scaffale.AllowUserToAddRows = false;
            this.dgw_Scaffale.AllowUserToDeleteRows = false;
            this.dgw_Scaffale.AllowUserToResizeColumns = false;
            this.dgw_Scaffale.AllowUserToResizeRows = false;
            this.dgw_Scaffale.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgw_Scaffale.BackgroundColor = System.Drawing.Color.White;
            this.dgw_Scaffale.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgw_Scaffale.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.DimGray;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgw_Scaffale.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgw_Scaffale.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgw_Scaffale.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cln_id,
            this.cln_Scaffale,
            this.cln_nome,
            this.cln_Qta,
            this.cln_note,
            this.cln_DataUlltima,
            this.cln_cancellaelemento});
            this.dgw_Scaffale.ContextMenuStrip = this.tls_aggiungioggetto;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgw_Scaffale.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgw_Scaffale.Location = new System.Drawing.Point(6, 347);
            this.dgw_Scaffale.Name = "dgw_Scaffale";
            this.dgw_Scaffale.ReadOnly = true;
            this.dgw_Scaffale.RowHeadersVisible = false;
            this.dgw_Scaffale.Size = new System.Drawing.Size(785, 275);
            this.dgw_Scaffale.TabIndex = 6;
            this.dgw_Scaffale.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Click_Celle);
            this.dgw_Scaffale.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dgw_Scaffale_CellFormatting);
            // 
            // cln_id
            // 
            this.cln_id.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            this.cln_id.DefaultCellStyle = dataGridViewCellStyle11;
            this.cln_id.HeaderText = "ID";
            this.cln_id.Name = "cln_id";
            this.cln_id.ReadOnly = true;
            this.cln_id.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.cln_id.Width = 30;
            // 
            // cln_Scaffale
            // 
            this.cln_Scaffale.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.cln_Scaffale.DefaultCellStyle = dataGridViewCellStyle12;
            this.cln_Scaffale.DividerWidth = 2;
            this.cln_Scaffale.HeaderText = "Sc.";
            this.cln_Scaffale.Name = "cln_Scaffale";
            this.cln_Scaffale.ReadOnly = true;
            this.cln_Scaffale.Width = 30;
            // 
            // cln_nome
            // 
            this.cln_nome.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            this.cln_nome.DefaultCellStyle = dataGridViewCellStyle13;
            this.cln_nome.HeaderText = "Nome";
            this.cln_nome.Name = "cln_nome";
            this.cln_nome.ReadOnly = true;
            this.cln_nome.Width = 70;
            // 
            // cln_Qta
            // 
            this.cln_Qta.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            this.cln_Qta.DefaultCellStyle = dataGridViewCellStyle14;
            this.cln_Qta.HeaderText = "Qta";
            this.cln_Qta.Name = "cln_Qta";
            this.cln_Qta.ReadOnly = true;
            this.cln_Qta.Width = 54;
            // 
            // cln_note
            // 
            dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopLeft;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            this.cln_note.DefaultCellStyle = dataGridViewCellStyle15;
            this.cln_note.HeaderText = "Note";
            this.cln_note.Name = "cln_note";
            this.cln_note.ReadOnly = true;
            // 
            // cln_DataUlltima
            // 
            this.cln_DataUlltima.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            dataGridViewCellStyle16.BackColor = System.Drawing.Color.WhiteSmoke;
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            this.cln_DataUlltima.DefaultCellStyle = dataGridViewCellStyle16;
            this.cln_DataUlltima.HeaderText = "DataUltimaModifica";
            this.cln_DataUlltima.Name = "cln_DataUlltima";
            this.cln_DataUlltima.ReadOnly = true;
            this.cln_DataUlltima.Width = 151;
            // 
            // cln_cancellaelemento
            // 
            this.cln_cancellaelemento.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.cln_cancellaelemento.ContextMenuStrip = this.tls_riga_opzioni;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            this.cln_cancellaelemento.DefaultCellStyle = dataGridViewCellStyle17;
            this.cln_cancellaelemento.HeaderText = "";
            this.cln_cancellaelemento.Name = "cln_cancellaelemento";
            this.cln_cancellaelemento.ReadOnly = true;
            this.cln_cancellaelemento.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.cln_cancellaelemento.Text = ">";
            this.cln_cancellaelemento.Width = 5;
            // 
            // tls_riga_opzioni
            // 
            this.tls_riga_opzioni.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tls_riga_opzioni_AggiungiRimuoviQuant,
            this.toolStripSeparator1,
            this.tls_riga_opzioni_CancellaRiga,
            this.toolStripSeparator2,
            this.tls_riga_opzioni_Sposta,
            this.toolStripSeparator4,
            this.tls_riga_opzioni_Annulla});
            this.tls_riga_opzioni.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.tls_riga_opzioni.Name = "tls_riga_opzioni";
            this.tls_riga_opzioni.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tls_riga_opzioni.ShowImageMargin = false;
            this.tls_riga_opzioni.Size = new System.Drawing.Size(148, 110);
            this.tls_riga_opzioni.Opened += new System.EventHandler(this.tls_riga_opzioni_Opened);
            // 
            // tls_riga_opzioni_AggiungiRimuoviQuant
            // 
            this.tls_riga_opzioni_AggiungiRimuoviQuant.Name = "tls_riga_opzioni_AggiungiRimuoviQuant";
            this.tls_riga_opzioni_AggiungiRimuoviQuant.Size = new System.Drawing.Size(147, 22);
            this.tls_riga_opzioni_AggiungiRimuoviQuant.Text = "Modifica Oggetto";
            this.tls_riga_opzioni_AggiungiRimuoviQuant.Click += new System.EventHandler(this.tls_riga_opzioni_AggiungiRimuoviQuant_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(144, 6);
            // 
            // tls_riga_opzioni_CancellaRiga
            // 
            this.tls_riga_opzioni_CancellaRiga.Name = "tls_riga_opzioni_CancellaRiga";
            this.tls_riga_opzioni_CancellaRiga.Size = new System.Drawing.Size(147, 22);
            this.tls_riga_opzioni_CancellaRiga.Text = "Cancella Riga..";
            this.tls_riga_opzioni_CancellaRiga.Click += new System.EventHandler(this.tls_riga_opzioni_CancellaRiga_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(144, 6);
            // 
            // tls_riga_opzioni_Sposta
            // 
            this.tls_riga_opzioni_Sposta.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tls_riga_opzioni_sposta_ScegliScaffale,
            this.tls_riga_opzioni_sposta_conferma});
            this.tls_riga_opzioni_Sposta.Name = "tls_riga_opzioni_Sposta";
            this.tls_riga_opzioni_Sposta.Size = new System.Drawing.Size(147, 22);
            this.tls_riga_opzioni_Sposta.Text = "Sposta in Scaffale..";
            // 
            // tls_riga_opzioni_sposta_ScegliScaffale
            // 
            this.tls_riga_opzioni_sposta_ScegliScaffale.FlatStyle = System.Windows.Forms.FlatStyle.Standard;
            this.tls_riga_opzioni_sposta_ScegliScaffale.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tls_riga_opzioni_sposta_ScegliScaffale.Name = "tls_riga_opzioni_sposta_ScegliScaffale";
            this.tls_riga_opzioni_sposta_ScegliScaffale.Size = new System.Drawing.Size(121, 25);
            // 
            // tls_riga_opzioni_sposta_conferma
            // 
            this.tls_riga_opzioni_sposta_conferma.Name = "tls_riga_opzioni_sposta_conferma";
            this.tls_riga_opzioni_sposta_conferma.Size = new System.Drawing.Size(181, 22);
            this.tls_riga_opzioni_sposta_conferma.Text = "Conferma";
            this.tls_riga_opzioni_sposta_conferma.Click += new System.EventHandler(this.tls_riga_opzioni_sposta_conferma_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(144, 6);
            // 
            // tls_riga_opzioni_Annulla
            // 
            this.tls_riga_opzioni_Annulla.Name = "tls_riga_opzioni_Annulla";
            this.tls_riga_opzioni_Annulla.Size = new System.Drawing.Size(147, 22);
            this.tls_riga_opzioni_Annulla.Text = "Annulla..";
            // 
            // tls_aggiungioggetto
            // 
            this.tls_aggiungioggetto.DropShadowEnabled = false;
            this.tls_aggiungioggetto.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tls_nuovo_aggiungiogg,
            this.toolStripSeparator3,
            this.tls_nuovo_aggiorna});
            this.tls_aggiungioggetto.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.VerticalStackWithOverflow;
            this.tls_aggiungioggetto.Name = "tls_aggiungioggetto";
            this.tls_aggiungioggetto.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.tls_aggiungioggetto.ShowImageMargin = false;
            this.tls_aggiungioggetto.Size = new System.Drawing.Size(152, 54);
            // 
            // tls_nuovo_aggiungiogg
            // 
            this.tls_nuovo_aggiungiogg.Enabled = false;
            this.tls_nuovo_aggiungiogg.Name = "tls_nuovo_aggiungiogg";
            this.tls_nuovo_aggiungiogg.Size = new System.Drawing.Size(151, 22);
            this.tls_nuovo_aggiungiogg.Text = "Aggiungi Oggetto..";
            this.tls_nuovo_aggiungiogg.Click += new System.EventHandler(this.tls_nuovo_aggiungiogg_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(148, 6);
            // 
            // tls_nuovo_aggiorna
            // 
            this.tls_nuovo_aggiorna.Enabled = false;
            this.tls_nuovo_aggiorna.Name = "tls_nuovo_aggiorna";
            this.tls_nuovo_aggiorna.Size = new System.Drawing.Size(151, 22);
            this.tls_nuovo_aggiorna.Text = "Aggiorna";
            this.tls_nuovo_aggiorna.TextDirection = System.Windows.Forms.ToolStripTextDirection.Horizontal;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aiutoToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.menuStrip1.Size = new System.Drawing.Size(799, 24);
            this.menuStrip1.TabIndex = 7;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.esciToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cancellaOggettiToolStripMenuItem,
            this.cancellaScaffaliToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(108, 22);
            this.toolStripMenuItem1.Text = "Pulisci";
            // 
            // cancellaOggettiToolStripMenuItem
            // 
            this.cancellaOggettiToolStripMenuItem.Name = "cancellaOggettiToolStripMenuItem";
            this.cancellaOggettiToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.cancellaOggettiToolStripMenuItem.Text = "Cancella Oggetti";
            this.cancellaOggettiToolStripMenuItem.Click += new System.EventHandler(this.cancellaOggettiToolStripMenuItem_Click);
            // 
            // cancellaScaffaliToolStripMenuItem
            // 
            this.cancellaScaffaliToolStripMenuItem.Name = "cancellaScaffaliToolStripMenuItem";
            this.cancellaScaffaliToolStripMenuItem.Size = new System.Drawing.Size(212, 22);
            this.cancellaScaffaliToolStripMenuItem.Text = "Cancella Oggetti e Scaffali";
            this.cancellaScaffaliToolStripMenuItem.Click += new System.EventHandler(this.cancellaScaffaliToolStripMenuItem_Click);
            // 
            // esciToolStripMenuItem
            // 
            this.esciToolStripMenuItem.Name = "esciToolStripMenuItem";
            this.esciToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.esciToolStripMenuItem.Text = "Esci";
            this.esciToolStripMenuItem.Click += new System.EventHandler(this.esciToolStripMenuItem_Click);
            // 
            // aiutoToolStripMenuItem
            // 
            this.aiutoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.toolStripSeparator7,
            this.creditiToolStripMenuItem});
            this.aiutoToolStripMenuItem.Name = "aiutoToolStripMenuItem";
            this.aiutoToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.aiutoToolStripMenuItem.Text = "Aiuto";
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(109, 22);
            this.toolStripMenuItem3.Text = "Aiuto";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(106, 6);
            // 
            // creditiToolStripMenuItem
            // 
            this.creditiToolStripMenuItem.Name = "creditiToolStripMenuItem";
            this.creditiToolStripMenuItem.Size = new System.Drawing.Size(109, 22);
            this.creditiToolStripMenuItem.Text = "Crediti";
            this.creditiToolStripMenuItem.Click += new System.EventHandler(this.creditiToolStripMenuItem_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            // 
            // lbl_nessunelemento
            // 
            this.lbl_nessunelemento.AutoSize = true;
            this.lbl_nessunelemento.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_nessunelemento.Location = new System.Drawing.Point(289, 479);
            this.lbl_nessunelemento.Name = "lbl_nessunelemento";
            this.lbl_nessunelemento.Size = new System.Drawing.Size(222, 20);
            this.lbl_nessunelemento.TabIndex = 8;
            this.lbl_nessunelemento.Text = "Nessun Scaffale Selezionato! ";
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.dataGridViewImageColumn1.HeaderText = "";
            // this.dataGridViewImageColumn1.Image = global::GestioneMagazzino.Properties.Resources.cancella;
            this.dataGridViewImageColumn1.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.ReadOnly = true;
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(799, 662);
            this.Controls.Add(this.lbl_nessunelemento);
            this.Controls.Add(this.dgw_Scaffale);
            this.Controls.Add(this.lstw_scaffali);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.label2);
            this.MainMenuStrip = this.menuStrip1;
            this.MaximumSize = new System.Drawing.Size(815, 701);
            this.MinimumSize = new System.Drawing.Size(815, 701);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gestione Magazzino";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tls_Scaffali.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgw_Scaffale)).EndInit();
            this.tls_riga_opzioni.ResumeLayout(false);
            this.tls_aggiungioggetto.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView lstw_scaffali;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.DataGridView dgw_Scaffale;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cancellaOggettiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cancellaScaffaliToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem esciToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aiutoToolStripMenuItem;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private ContextMenuStrip tls_riga_opzioni;
        private ToolStripMenuItem tls_riga_opzioni_AggiungiRimuoviQuant;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem tls_riga_opzioni_CancellaRiga;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripMenuItem tls_riga_opzioni_Annulla;
        private ToolTip toolTip1;
        private ContextMenuStrip tls_aggiungioggetto;
        private ToolStripMenuItem tls_nuovo_aggiungiogg;
        private ToolStripSeparator toolStripSeparator3;
        private ToolStripMenuItem tls_nuovo_aggiorna;
        private ToolStripMenuItem tls_riga_opzioni_Sposta;
        private ToolStripComboBox tls_riga_opzioni_sposta_ScegliScaffale;
        private ToolStripMenuItem tls_riga_opzioni_sposta_conferma;
        private ToolStripSeparator toolStripSeparator4;
        private DataGridViewTextBoxColumn cln_id;
        private DataGridViewTextBoxColumn cln_Scaffale;
        private DataGridViewTextBoxColumn cln_nome;
        private DataGridViewTextBoxColumn cln_Qta;
        private DataGridViewTextBoxColumn cln_note;
        private DataGridViewTextBoxColumn cln_DataUlltima;
        private DataGridViewButtonColumn cln_cancellaelemento;
        private Label lbl_nessunelemento;
        private ContextMenuStrip tls_Scaffali;
        private ToolStripMenuItem tls_scaffali_aggiungi;
        private ToolStripSeparator toolStripSeparator5;
        private ToolStripMenuItem tls_scaffali_modifica;
        private ToolStripComboBox tls_scaffali_modifica_scegliscaffale;
        private ToolStripMenuItem tls_scaffali_modifica_conferma;
        private ToolStripSeparator toolStripSeparator6;
        private ToolStripMenuItem tls_scaffali_elimina;
        private ToolStripComboBox tls_scaffali_elimina_scegliscaffale;
        private ToolStripMenuItem tls_scaffali_elimina_conferma;
        private ToolStripMenuItem toolStripMenuItem3;
        private ToolStripSeparator toolStripSeparator7;
        private ToolStripMenuItem creditiToolStripMenuItem;

    }
}

