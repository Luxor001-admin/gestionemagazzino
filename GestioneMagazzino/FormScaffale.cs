﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GestioneMagazzino
{
    public partial class FormScaffale : Form
    {
        public string nome;
        public string nota;
        public bool valido = false;
        public int ID;
        public Modalità modalitàform;
        public enum Modalità
        {
            Modifica,
            Nuovo
        }
        public FormScaffale()
        {
            modalitàform = Modalità.Nuovo;
            InitializeComponent();
            this.Text = "Aggiungi Scaffale";
        }
        public FormScaffale(string nome,string nota,int ID)
        {
            modalitàform = Modalità.Modifica;
            InitializeComponent();
            this.Text = "Aggiungi Scaffale";
            this.nome = nome;
            this.nota = nota;
            this.ID = ID;
        }

        private void btn_conferma_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt_nome.Text))
            {
                MessageBox.Show("Lo scaffale non ha un nome inserito!", "Errore!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            nome = txt_nome.Text;
            nota = txt_note.Text;
            valido = true;
            this.Close();
        }

        private void btn_annulla_Click(object sender, EventArgs e)
        {
            valido = false;
            this.Close();
        }

        private void Scaffale_Load(object sender, EventArgs e)
        {
            txt_nome.Text = nome;
            txt_note.Text = nota;
        }
    }
}
